package com.graphql.demo.resolve;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.graphql.demo.model.entity.Author;
import com.graphql.demo.model.entity.Post;
import com.graphql.demo.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PostResolver implements GraphQLResolver<Post> {

    private final AuthorService authorService;

    @Autowired
    public PostResolver(AuthorService authorService) {
        this.authorService = authorService;
    }

    public Author getAuthor(Post post) {
        return authorService.getOneAuthor(post.getAuthor().getId());
    }

}
