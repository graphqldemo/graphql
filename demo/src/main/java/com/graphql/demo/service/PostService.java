package com.graphql.demo.service;

import com.graphql.demo.model.entity.Author;
import com.graphql.demo.model.entity.Post;
import com.graphql.demo.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostService {

    private final PostRepository postRepository;
    private final AuthorService authorService;

    @Autowired
    public PostService(PostRepository postRepository, AuthorService authorService) {
        this.postRepository = postRepository;
        this.authorService = authorService;
    }

    public Post createPost(Post post, Integer authorId) {
        Author author = authorService.getOneAuthor(authorId);
        post.setAuthor(author);
        return postRepository.save(post);
    }

    public List<Post> getPost() {
        return postRepository.findAll();
    }

    public Post getOnePost(Integer id) {
        return postRepository.findById(id).orElse(null);
    }
}
