package com.graphql.demo.service;

import com.graphql.demo.model.entity.Author;
import com.graphql.demo.repository.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthorService {

    private final AuthorRepository authorRepository;

    @Autowired
    public AuthorService(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    public Author createAuthor(Author author) {
//        Author newAuthor = new Author(null, name, thumbnail);
        return authorRepository.save(author);
    }

    public List<Author> getAuthors() {
        return authorRepository.findAll();
    }

    public Author getOneAuthor(Integer id) {
        return authorRepository.findById(id).orElse(null);
    }
}
