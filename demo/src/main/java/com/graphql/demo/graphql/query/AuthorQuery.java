package com.graphql.demo.graphql.query;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.graphql.demo.model.entity.Author;
import com.graphql.demo.model.entity.Post;
import com.graphql.demo.service.AuthorService;
import com.graphql.demo.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AuthorQuery implements GraphQLQueryResolver {


    private final AuthorService authorService;

    @Autowired
    public AuthorQuery(AuthorService authorService) {
        this.authorService = authorService;
    }


    public List<Author> allAuthors(){

        return authorService.getAuthors();
    }

    public Author oneAuthor(Integer id){

        return authorService.getOneAuthor(id);
    }

}
