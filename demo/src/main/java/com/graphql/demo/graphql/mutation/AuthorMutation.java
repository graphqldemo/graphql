package com.graphql.demo.graphql.mutation;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.graphql.demo.model.entity.Author;
import com.graphql.demo.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AuthorMutation implements GraphQLMutationResolver {

    private final AuthorService authorService;

    @Autowired
    public AuthorMutation(AuthorService authorService) {
        this.authorService = authorService;
    }

    public Author createAuthor(Author author) {
        return authorService.createAuthor(author);
    }
}
