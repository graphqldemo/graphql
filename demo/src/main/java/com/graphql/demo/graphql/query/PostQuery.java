package com.graphql.demo.graphql.query;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.graphql.demo.model.entity.Post;
import com.graphql.demo.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PostQuery implements GraphQLQueryResolver {
    private final PostService postService;

    @Autowired
    public PostQuery(PostService postService) {
        this.postService = postService;
    }


    public List<Post> allPosts(){

        return postService.getPost();
    }

    public Post onePost(Integer id){

        return postService.getOnePost(id);
    }
}
