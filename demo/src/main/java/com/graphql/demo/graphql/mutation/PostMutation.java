package com.graphql.demo.graphql.mutation;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.graphql.demo.model.entity.Author;
import com.graphql.demo.model.entity.Post;
import com.graphql.demo.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PostMutation implements GraphQLMutationResolver {

    private final PostService postService;

    @Autowired
    public PostMutation(PostService postService) {
        this.postService = postService;
    }

    public Post createPost(Post post, Integer authorId) {
        return postService.createPost(post, authorId);
    }
}
